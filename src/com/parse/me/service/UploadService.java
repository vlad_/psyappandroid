package com.parse.me.service;

import java.util.Queue;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.parse.me.a.ExperimentView;
import com.parse.me.a.HttpRequestsHandler;
import com.parse.me.a.MainPsyAppActivity;
import com.parse.me.a.R;
import com.parse.me.a.Models.ExperimentResponse;
import com.parse.me.database.DatabaseHandler;

public class UploadService extends Service {

	private static final int ALL_OK = 1;
	private static final String TAG = "UploadService";
	ExperimentResponse response;
	int postCode = 0;
	int postCode2 = 0;
	DatabaseHandler db = new DatabaseHandler(UploadService.this);

	Thread syncDbExpThread = new Thread() {
		public void run() {

			// Phase 2 includes database sync

			Queue<ExperimentResponse> queue = db.returnUnsynced();
			ExperimentResponse currentSyncing = queue.poll();
			System.out.println("QUEUE has :" + queue.size());
			int counter = 0;
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			if (queue.size() != 0) {
				while (currentSyncing != null) {

					postCode2 = 0;
					currentSyncing.set_synced("true");
					Notification notification = new Notification(
							R.drawable.ic_launcher,
							"Psyapp synchronizing database..(" + counter + "/"
									+ queue.size() + ")",
							System.currentTimeMillis());
					notification.setLatestEventInfo(
							getApplicationContext(),
							"Psyapp synchronizing database..",
							"Trying to sync experiment"
									+ currentSyncing.get_exp_id(),
							PendingIntent.getActivity(getApplicationContext(),
									0, new Intent(getApplicationContext(),
											MainPsyAppActivity.class),
									PendingIntent.FLAG_UPDATE_CURRENT));
					mNotificationManager.notify(323, notification);

					while (postCode2 == 0) {
						try {
							Log.i(TAG, "Checking if db online..");
							if (isOnline()) {
								Message myMessage = new Message();
								Bundle data = new Bundle();
								data.putSerializable("response", currentSyncing);
								myMessage.setData(data);
								dbSyncer.sendMessage(myMessage);
								Log.i(TAG, "Message sent");
								sleep(9000);
							} else {
								sleep(30000);
							}

						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					counter++;
					currentSyncing = queue.poll();

				}

			}
			mNotificationManager.cancel(323);
			stopSelf();
		}

	};

	Thread syncLatestExpThread = new Thread() {
		public void run() {

			// Phase 1 includes the last experiment syncing
			while (postCode == 0) {
				try {
					Log.i(TAG, "Checking if online..");
					if (isOnline()) {
						Message myMessage = new Message();

						currentSyncer.sendMessage(myMessage);
						Log.i(TAG, "Message sent");
					}
						sleep(30000);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			syncDbExpThread.start();

		}

	};

	private boolean isOnline() {
		// TODO Auto-generated method stub
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;

	}

	private Handler currentSyncer = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Log.i(TAG, "Message received");
			HttpRequestsHandler post = new HttpRequestsHandler();
			System.out.println("Sync response:"+response.get_id());
			if (post.postData(response.get_exp_id(), response.get_response()) == 1) {
				Toast.makeText(getApplicationContext(), "Response sent",
						Toast.LENGTH_LONG).show();
				Log.i(TAG, "Response sent");
				// Change the sync flag to 1 that mean it's synced
				response.set_synced("true");
				db.deleteResponse(response);
				postCode = 200;

			}
		}
	};

	private Handler dbSyncer = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Log.i(TAG, "Db message received");
			Bundle data = msg.getData();
			ExperimentResponse responseDb = (ExperimentResponse) data
					.getSerializable("response");
			HttpRequestsHandler post = new HttpRequestsHandler();
			System.out.println("Sync response with id" + responseDb.get_id());
			if (post.postData(responseDb.get_exp_id(),
					responseDb.get_response()) == 1) {
				Toast.makeText(getApplicationContext(), "Response sent",
						Toast.LENGTH_LONG).show();
				Log.i(TAG, "Db Response sent");
				// Change the sync flag to 1 that mean it's synced
				responseDb.set_synced("true");
				db.deleteResponse(responseDb);
				postCode2 = 200;

			}
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub+
		// Might not be serializable

		return null;
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		Log.i(TAG, "OnStart called");
		response = (ExperimentResponse) intent.getSerializableExtra("response");
		System.out.println(response.get_exp_id());
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

		// // TODO Auto-generated method stub
		//
		// HttpRequestsHandler post = new HttpRequestsHandler();
		// int requestCode = -1;
		// while (requestCode == -1) {
		// if (post.postData(response.get_exp_id(), response.get_response()) ==
		// ALL_OK) {
		// requestCode = 1;
		// }
		// }
		syncLatestExpThread.start();

	}

}
