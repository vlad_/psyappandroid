package com.parse.me.database;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.parse.me.a.Models.ExperimentResponse;
import com.parse.me.a.Models.StoredExperiment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static String TAG = "DatabaseHandler";
	private static int DATABASE_VERSION = 1;
	private static String DATABASE_NAME = "ResponsesDB";
	private static String TABLE_RESPONSES = "responses";
	private static String TABLE_EXPERIMENTS = "experiments";
	private static String TABLE_ALARMS = "alarms";

	// Response Table Columns names
	private static final String KEY_ID = "_id";
	private static final String KEY_EXP_ID = "exp_id";
	private static final String KEY_RESPONSE = "response";
	private static final String KEY_SYNCED = "synced";

	// Experiment Table Columns names
	private static final String EXP_ID = "_id";
	private static final String ID_OF_EXP = "exp_id";
	private static final String GPS = "gps_enabled";
	private static final String LOG = "log";
	private static final String REPEAT = "repeatable";

	// Alarm Table Column names
	private static final String ALARM_ID = "_id";
	private static final String ID_OF_EXP_TO_ALARM = "alarm_id";
	// The unique id will be used to remove the experiment from the db after
	// it's finished
	private static final String UNIQUE_EXPERIMENT_ID_FOR_NOT = "uid_for_not";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_RESPONSES_TABLE = "CREATE TABLE " + TABLE_RESPONSES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_EXP_ID
				+ " TEXT," + KEY_RESPONSE + " BLOB," + KEY_SYNCED + " TEXT"
				+ ")";
		db.execSQL(CREATE_RESPONSES_TABLE);

		String CREATE_EXPERIMENT_TABLE = "CREATE TABLE " + TABLE_EXPERIMENTS
				+ "(" + EXP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ ID_OF_EXP + " TEXT," + GPS + " INTEGER," + LOG + " TEXT,"
				+ REPEAT + " INTEGER" + ")";
		db.execSQL(CREATE_EXPERIMENT_TABLE);

		String CREATE_ALARM_TABLE = "CREATE TABLE " + TABLE_ALARMS + "("
				+ ALARM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ ID_OF_EXP_TO_ALARM + " TEXT," + UNIQUE_EXPERIMENT_ID_FOR_NOT
				+ " TEXT" + ")";
		db.execSQL(CREATE_ALARM_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESPONSES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPERIMENTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARMS);

		// Create tables again
		onCreate(db);
	}

	public void addExperiment(StoredExperiment storedExperiment) {
		SQLiteDatabase db = this.getWritableDatabase();
		// set the values to be inserted
		ContentValues values = new ContentValues();
		values.put(ID_OF_EXP, storedExperiment.getExp_id());
		values.put(GPS, storedExperiment.isGps_enabled());
		values.put(LOG, storedExperiment.getLog());
		values.put(REPEAT, storedExperiment.isRepeatable());
		// insert values
		db.insert(TABLE_EXPERIMENTS, null, values);
		db.close();
	}

	// Adding new response.Takes as input the experiment response object
	public void addResponse(ExperimentResponse response) {

		// Get instance of the db
		SQLiteDatabase db = this.getWritableDatabase();
		// Get the values from the response obj and make a COntentValue
		ContentValues values = new ContentValues();
		values.put(KEY_EXP_ID, response.get_exp_id());
		values.put(KEY_RESPONSE, response.get_response());
		values.put(KEY_SYNCED, response.get_synced());

		// Insert values in the database
		db.insert(TABLE_RESPONSES, null, values);
		db.close();// Close the db
	}

	// Adding alarm clock to the db.
	public void addAlarmToBePulled(String idOfExpToAlarm, String UID_for_not) {
		// Get instance of the db
		SQLiteDatabase db = this.getWritableDatabase();
		// Get the values from the response obj and make a COntentValue
		ContentValues values = new ContentValues();
		values.put(ID_OF_EXP_TO_ALARM, idOfExpToAlarm);
		values.put(UNIQUE_EXPERIMENT_ID_FOR_NOT, UID_for_not);
		// Insert them i said
		db.insert(TABLE_ALARMS, null, values);
		db.close();
	}

	/**
	 * Getting the number of alarms
	 * 
	 * @return
	 */
	public int getAllAlarms() {
		SQLiteDatabase db = this.getReadableDatabase();
		// Cursor cursor = db.query(TABLE_ALARMS, new String[] { ALARM_ID,
		// ID_OF_EXP_TO_ALARM },null,
		// null, null, null, null, null);
		Cursor cursor = db.query(TABLE_ALARMS, new String[] { ALARM_ID,
				ID_OF_EXP_TO_ALARM }, null, null, null, null, null);
		if (cursor.moveToFirst() == true) {
			System.out.println("Cursora:" + cursor.getString(0));
			System.out.println("Cursora:" + cursor.getString(1));
			while (cursor.moveToNext() == true) {
				System.out.println("Cursora:" + cursor.getString(0));
				System.out.println("Cursora:" + cursor.getString(1));
			}
		}

		db.close();
		return cursor.getCount();
	}

	/**
	 * Delete the alarm
	 * 
	 * @return
	 */
	public void deleteAlarm(String id) {

		SQLiteDatabase db = this.getWritableDatabase();
		System.out.println("Trying to delete" + id);
		db.execSQL("DELETE FROM " + TABLE_ALARMS + " WHERE "
				+ UNIQUE_EXPERIMENT_ID_FOR_NOT + " = " + id);
		db.close();

	}

	/**
	 * GET the last entity of the alarmDB which is the experiment that is more
	 * late that the other experiments
	 * 
	 * @return
	 */
	public String[] getLastAlarm() {
		// Local values
		String idOfLast = "";
		String idOfLastExperiment = "";
		String idOfUnique = "";

		// Get db instance
		SQLiteDatabase db = this.getReadableDatabase();
		// Create the cursor
		// String myQuery = "SELECT MIN(_id),uid_for_not,alarm_id FROM alarms";
		// Cursor cursor = db.rawQuery(myQuery, null);

		Cursor cursor = db.query(TABLE_ALARMS, new String[] { ALARM_ID,
				UNIQUE_EXPERIMENT_ID_FOR_NOT, ID_OF_EXP_TO_ALARM }, null, null,
				null, null, null);

		if (cursor.moveToFirst()) {

			idOfLast = cursor.getString(0);
			idOfLastExperiment = cursor.getString(2);
			idOfUnique = cursor.getString(1);

			cursor.close();
		}

		db.close();
		String[] alarmValues = { idOfLastExperiment, idOfUnique };
		return alarmValues;

	}

	public Cursor getAllResponsesCursor() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_RESPONSES, new String[] { KEY_ID,
				KEY_EXP_ID, KEY_RESPONSE }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		db.close();
		return cursor;
	}

	public Cursor getAllExperimentCursor() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_EXPERIMENTS, new String[] { EXP_ID,
				ID_OF_EXP, REPEAT, LOG }, null, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		db.close();
		return cursor;
	}

	public Queue<ExperimentResponse> returnUnsynced() {

		Log.i(TAG, "GetUnSyncCount");
		Queue<ExperimentResponse> queueWithResponses = new LinkedList<ExperimentResponse>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_RESPONSES, new String[] { KEY_ID,
				KEY_EXP_ID, KEY_SYNCED, KEY_RESPONSE }, KEY_SYNCED + "=?",
				new String[] { "false" }, null, null, null, null);

		if (cursor.moveToFirst()) {
			while (cursor.moveToNext()) {
				ExperimentResponse response = new ExperimentResponse();
				response.set_id(Integer.parseInt(cursor.getString(0)));
				response.set_exp_id(cursor.getString(1));
				response.set_synced(cursor.getString(2));
				response.set_response(cursor.getString(3));
				// Adding contact to list
				queueWithResponses.add(response);

				Log.i(TAG, "Counting...");
			}
		}
		db.close();

		return queueWithResponses;

	}

	public boolean checkIfExpExists(String id) {

		boolean status = false;

		SQLiteDatabase db = this.getReadableDatabase();
		// Create the cursor
		Cursor cursor = db.query(TABLE_EXPERIMENTS, new String[] { KEY_ID,
				KEY_EXP_ID, ID_OF_EXP }, ID_OF_EXP + "=?", new String[] { id },
				null, null, null, null);

		if (cursor.moveToFirst()) {
			if (cursor.getCount() >= 1) {
				status = true;
			} else {
				status = false;
			}
			db.close();

		} else {
			db.close();
			status = false;
		}

		
		cursor.close();
		
		return status;

	}

	// Getting single contact
	public ExperimentResponse getResponse(int id) {
		// Get db instance
		SQLiteDatabase db = this.getReadableDatabase();
		// Create the cursor
		Cursor cursor = db.query(TABLE_RESPONSES, new String[] { KEY_ID,
				KEY_EXP_ID, KEY_RESPONSE, KEY_SYNCED }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		ExperimentResponse response = new ExperimentResponse(
				cursor.getString(1), cursor.getString(2), cursor.getString(3));
		// return contact
		db.close();
		return response;
	}

	// Getting single contact
	public String getExperimentLog(int id) {
		// Get db instance
		SQLiteDatabase db = this.getReadableDatabase();
		// Create the cursor
		Cursor cursor = db.query(TABLE_EXPERIMENTS, new String[] { EXP_ID, LOG,
				KEY_ID }, ID_OF_EXP + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		String log = null;
		System.out.println("CURSOR" + cursor.getColumnCount());

		if (cursor.moveToFirst()) {

			System.out.println("CURSOR" + cursor.getColumnCount());
			log = cursor.getString(1);

		} else {
			Log.w(TAG, "The log is NULL");

		}

		cursor.close();
		db.close();
		return log;
	}

	// Getting All Contacts
	public List<ExperimentResponse> getAllResponses() {

		List<ExperimentResponse> responsesList = new ArrayList<ExperimentResponse>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * FROM " + TABLE_RESPONSES, null);

		if (cursor.moveToFirst()) {
			while (cursor.moveToNext()) {
				ExperimentResponse response = new ExperimentResponse();
				response.set_id(Integer.parseInt(cursor.getString(0)));
				response.set_exp_id(cursor.getString(1));
				response.set_response(cursor.getString(2));
				// Adding contact to list
				responsesList.add(response);

			}
		}
		db.close();
		return responsesList;

	}

	// Getting contacts Count
	public int getContactsCount() {

		Log.i(TAG, "GetContactsCount");
		List<ExperimentResponse> responsesList = new ArrayList<ExperimentResponse>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("Select * FROM " + TABLE_RESPONSES, null);

		if (cursor.moveToFirst()) {
			while (cursor.moveToNext()) {
				ExperimentResponse response = new ExperimentResponse();
				response.set_id(Integer.parseInt(cursor.getString(0)));
				response.set_exp_id(cursor.getString(1));
				response.set_response(cursor.getString(2));
				// Adding contact to list
				responsesList.add(response);
				Log.i(TAG, "Counting...");
			}
		}
		db.close();

		return responsesList.size();
	}

	// Updating single contact
	public int updateContact(ExperimentResponse contact) {
		return 0;
	}

	// Deleting single contact
	public void deleteResponse(ExperimentResponse response) {

		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_RESPONSES + " WHERE " + KEY_ID
				+ " = " + response.get_id());
		db.close();
	}

	public static String getKeyId() {
		return KEY_ID;
	}

	public static String getKeyExpId() {
		return KEY_EXP_ID;
	}

	public static String getKeyResponse() {
		return KEY_RESPONSE;
	}

	public static String getKeySynced() {
		return KEY_SYNCED;
	}

	public static String getExpId() {
		return EXP_ID;
	}

	public static String getIdOfExp() {
		return ID_OF_EXP;
	}

	public static String getGps() {
		return GPS;
	}

	public static String getLog() {
		return LOG;
	}

	public static String getRepeat() {
		return REPEAT;
	}

	public static String getDATABASE_NAME() {
		return DATABASE_NAME;
	}
}
