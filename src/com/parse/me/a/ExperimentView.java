package com.parse.me.a;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.Thread.State;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.parse.me.a.R;
import com.parse.me.a.Models.ExperimentResponse;
import com.parse.me.a.Models.Frame;
import com.parse.me.database.DatabaseHandler;
import com.parse.me.service.UploadService;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 * 
 */
public class ExperimentView extends Activity implements OnClickListener,
		OnTouchListener {
	/** Called when the activity is first created. */
	LinearLayout allFrame;
	TextView TextPlace;
	ImageView ImagePlace;
	Button bL, bR, resume;
	Boolean isItRunning;
	ImageView touchSurface;

	Button submitFreeInput;
	TextView QuestFreeText;
	EditText editFreeText;

	Button submitSeekBar;
	SeekBar seekBar;
	TextView seekBarText;
	TextView minSeek;
	TextView maxSeek;
	TextView currentSeek;
	ImageView cameraImageView;
	static String seekBarValueString;

	TableLayout checkBoxPlace;
	Button submitCheckBox;
	TextView QuestionCheckBox;
	int checkid = 0;
	CheckBox chk;
	TableRow tr;

	Button submitRadioButton;
	TextView QuestionRadioButton;
	RadioGroup radioGroup;
	RadioButton rdb;

	Button submitSpinner;
	TextView QuestionSpinner;
	Spinner spinner;
	BufferedWriter writer;
	BufferedReader reader;
	JsonArray frameLogArray;

	public String gpsEnabled;
	static final int GPSHANDLER_ACTIVITY_RETURN = 0;
	private boolean paused = false;

	HashMap<String, Bitmap> ImageMap = new HashMap<String, Bitmap>();
	static int nextFrameId1;
	static int nextFrameId2;
	static int buttonPressed;

	int framePresses;
	int currentPresses = 1;

	Boolean isItTouchFrame = false;
	String experimentId;
	// TODO implement the row of the frames
	List<List<String>> listOfTrials;
	List<Frame> frameArray;
	List<String> trial;
	ProgressDialog progreDialog0;

	Thread t1;
	static String log = "";
	long timeStarted = 0;
	private final static int NOTIFICATION_ID = 323;
	private String UID_OF_ALARM_FOR_NOT = null;

	/*************************/
	/** Variables for the log **/
	/*************************/

	protected String frameload;
	protected String buttonPress;
	protected String touchCords;
	protected String times;
	protected String dateNow;
	private Uri imageUri;
	private static final String TAG = "ExperimentView";
	private static final int CAMERA_INTENT_ID = 210;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resumedialogbox);
		initiate();

		Bundle params = getIntent().getExtras();
		experimentId = params.getString("exp_id");
		this.UID_OF_ALARM_FOR_NOT = params.getString("UID_FOR_NOT");
		System.out.println("UID4:" + UID_OF_ALARM_FOR_NOT);
		gpsEnabled = params.getString("gpsEnabled");
		this.frameArray = (List<Frame>) params.get("Frames");
		this.listOfTrials = (List<List<String>>) params.get("Trials");
		this.frameLogArray = new JsonArray();
		// DEBUGGING
		System.out.println("Frame num:" + frameArray.size());
		new setUptheExperiment().execute();

		if (gpsEnabled != null) {
			if (gpsEnabled.equalsIgnoreCase("true")) {

				Intent gpsHandler = new Intent(ExperimentView.this,
						GpsHandler.class);
				startActivityForResult(gpsHandler, GPSHANDLER_ACTIVITY_RETURN);

			}
		}

	}

	public void initiate() {

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		resume = (Button) findViewById(R.id.resume);
		resume.setOnClickListener(this);

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bL:

			try {
				writer.write("Button pressed :Left Button at:"
						+ (System.currentTimeMillis() - timeStarted + " ")
						+ ",");

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "Button pressed :Left Button at:"
			// + (System.currentTimeMillis() - timeStarted + " ") + "\n";
			if (nextFrameId1 != -1) {
				this.buttonPressed = R.id.bL;
			} else {
				this.buttonPressed = -1;
			}
			signalToContinue();
			break;
		case R.id.bR:
			try {
				writer.write("Button pressed :Right Button at:"
						+ (System.currentTimeMillis() - timeStarted + " " + ","));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "  Button pressed :Right Button at:"
			// + (System.currentTimeMillis() - timeStarted + " ") + "\n";
			if (nextFrameId1 != -1) {

				this.buttonPressed = R.id.bR;
			} else {
				this.buttonPressed = -1;
			}

			signalToContinue();
			break;
		case R.id.submitCheckBox:
			for (int i = 0; i < checkid; i++) {
				CheckBox chk = (CheckBox) findViewById(i);
				System.out.println(chk == null);
				if (chk != null) {
					try {
						writer.write(" " + chk.getText().toString() + " is "
								+ chk.isChecked() + ",");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// log = log + " " + chk.getText().toString() + " is "
					// + chk.isChecked();

				}

			}
			tr.removeAllViews();
			checkBoxPlace.removeAllViews();
			signalToContinue();
			break;
		case R.id.submitRadioBox:

			try {
				writer.write("The radioButton with id"
						+ radioGroup.getCheckedRadioButtonId() + " is checked"
						+ ",");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "The radioButton with id"
			// + radioGroup.getCheckedRadioButtonId() + " is checked";

			radioGroup.removeAllViews();

			signalToContinue();
			break;
		case R.id.submitSpinner:

			try {
				writer.write("Item with id "
						+ spinner.getSelectedItemPosition() + " was selected"
						+ ",");
				
				JsonObject jsonFrame = (JsonObject) frameLogArray
						.get(frameLogArray.size() - 1);
				    jsonFrame.addProperty("op", spinner.getSelectedItemPosition());
				  
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "Item with id " + spinner.getSelectedItemPosition()
			// + " was selected";

			signalToContinue();
			break;
		case R.id.submitFree:
			try {
				writer.write("Text input :"
						+ this.editFreeText.getText().toString() + ",");
				
				    JsonObject jsonFrame = (JsonObject) frameLogArray
						.get(frameLogArray.size() - 1);
				    jsonFrame.addProperty("op", this.editFreeText.getText().toString());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "Text input :" +
			// this.editFreeText.getText().toString();
			signalToContinue();
			break;
		case R.id.submitSeekBar:
			try {
				writer.write("Seek bar value :" + this.seekBarValueString + ",");
				JsonObject jsonFrame = (JsonObject) frameLogArray
						.get(frameLogArray.size() - 1);
				    jsonFrame.addProperty("op", this.seekBarValueString);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// log = log + "Seek bar value :" + this.seekBarValueString;
			signalToContinue();
			break;
		case R.id.resume:
			writer = null;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(
						openFileOutput("myfile", MODE_PRIVATE)));
				writer.write("START LOGGING..");
				System.out.println("START LOGGING..");
			} catch (Exception e) {
				e.printStackTrace();
			}
			startExperiment();
			break;
		default:
			break;
		}

	}

	/**
	 * FFS THE CODE IS SO FUCKING MESSED UP. PLEASE FIX IT PLZPLZPLZPZL(ALMOST
	 * DONE) The code is less fucked up than it used to be.I made it a bit more
	 * tide up. Now I want to implement a new recursive method in order to
	 * create trees of option frames
	 * 
	 * @author deauth
	 * 
	 */
	public class FrameRoll implements Runnable {

		public String finalText;

		public void run() {

			timeStarted = System.currentTimeMillis();
			dateNow = String.valueOf(new Date());

			try {
				writer.write("STARTED AT " + dateNow + " ");
			} catch (IOException e1) {
				// TODO AUTO-GENERATED CATCH BLOCK
				e1.printStackTrace();
			}
			// log = log + "Started at " + date + " ";

			for (List<String> trial : listOfTrials) {

				for (String fcall : trial) {

					final String s = fcall;

					// If it's an option frame
					if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("optionFrame") == true) {
						runOnUiThread(new Runnable() {

							public void run() {

								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "normalFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);
								// we will increase the jsonCounter on the
								// listener

								setNormalOptionFrame();
								viewFrame(Integer.parseInt(s));

							}
						});
						await();

						if (buttonPressed == R.id.bL) {
							runOnUiThread(new Runnable() {

								public void run() {
									JsonObject jsonFrame = (JsonObject) frameLogArray
											.get(frameLogArray.size() - 1);
									jsonFrame.addProperty("d", "b1");
									jsonFrame.addProperty("dtm",
											System.currentTimeMillis()
													- timeStarted);
									viewFrame(nextFrameId1);

								}
							});
							setFrameTimeLength(nextFrameId1);

						} else if (buttonPressed == R.id.bR) {
							runOnUiThread(new Runnable() {

								public void run() {
									JsonObject jsonFrame = (JsonObject) frameLogArray
											.get(frameLogArray.size() - 1);
									jsonFrame.addProperty("d", "b2");
									jsonFrame.addProperty("dtm",
											System.currentTimeMillis()
													- timeStarted);
									viewFrame(nextFrameId2);
								}
							});
							setFrameTimeLength(nextFrameId2);
						} else if (buttonPressed == -1) {
							System.out.println("MPOULOUS");
						}
						// If it's a normalFrame
					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("normalFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "normalFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setNormalOptionFrame();
								viewFrame(Integer.parseInt(idString));

							}

						});
						if (paused == false) {
							setFrameTimeLength(Integer.parseInt(s));
						} else {
							await();
						}

					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("touchFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "touchframe");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setTouchStuff(Integer.parseInt(idString));
								setPicturesforTouch(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});

					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("freeTextFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "freeTextFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setFreeTextFrame(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});
					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("seekBarFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "seekBarFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setSeekBarFrame(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});
					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("checkBoxFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "checkBoxFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setCheckBoxFrame(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});
					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("radioBoxFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "radioBoxFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setRadioBoxFrame(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});
					} else if (frameArray.get(Integer.parseInt(s) - 1).frame_type
							.equalsIgnoreCase("spinnerFrame") == true) {

						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {
								// THis will create the Json file and will
								// include
								// The id
								JsonObject jsonFrame = new JsonObject();
								jsonFrame.addProperty("id", s);
								jsonFrame.addProperty("tp", "spinnerFrame");
								jsonFrame.addProperty("tm",
										System.currentTimeMillis()
												- timeStarted);
								// add it to the array
								frameLogArray.add(jsonFrame);

								setSpinnerFrame(Integer.parseInt(idString));

							}

						});
						await();
						runOnUiThread(new Runnable() {

							String idString = s;

							public void run() {

								stopStuff(Integer.parseInt(idString));
							}

						});
					}
				}
			}

			runOnUiThread(new Runnable() {

				public void run() {

					TextPlace.setText("");
					ImagePlace.setImageDrawable(null);
					bL.setVisibility(View.INVISIBLE);
					bR.setVisibility(View.INVISIBLE);

				}
			});

			/**
			 * TODO BRING IT BACK TO HAVE GPS AGAIN
			 */

			if (gpsEnabled != null) {
				if (gpsEnabled.equalsIgnoreCase("true")) {

					runOnUiThread(new Runnable() {
						public void run() {

							Intent gpsHandler = new Intent(ExperimentView.this,
									GpsHandler.class);
							startActivityForResult(gpsHandler,
									GPSHANDLER_ACTIVITY_RETURN);
						}
					});

					await();
				}
			}

			// Write info of the phone
			try {
				writer.write("Finished at " + String.valueOf(new Date()) + " ");
				writer.write("Phone manufacturer :" + Build.MANUFACTURER);
				writer.write("Phone model :" + Build.MODEL);
				writer.write("Phone running android version :"
						+ Build.VERSION.RELEASE);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {

				writer.close();
				System.out.println("Writer Closed");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				reader = new BufferedReader(new InputStreamReader(
						openFileInput("myfile")));
				String line, results = "";
				while ((line = reader.readLine()) != null) {
					results += line;
				}

				log = log + results;

				/**
				 * TODO WRITE THE CODE FOR THE LOGFILE.TXT
				 */

				// SimpleDateFormat formatter = new SimpleDateFormat(
				// "yyyy_MM_dd_HH_mm_ss");
				// Date now = new Date();
				// String fileName = formatter.format(now) + ".txt";
				//
				// File experimentFolder = new File(
				// Environment.getExternalStorageDirectory() + "/Psyapp",
				// "Exp" + experimentId + "/logFiles");
				// File logtxt = new File(
				// Environment.getExternalStorageDirectory() + "/Psyapp",
				// "Exp" + experimentId + "/logFiles/" + fileName);
				// if (experimentFolder.exists()) {
				//
				// // Create the txt file
				// logtxt.createNewFile();
				// BufferedWriter buf = new BufferedWriter(new FileWriter(
				// logtxt, true));
				// buf.append(results);
				// buf.newLine();
				// buf.close();
				// } else {
				//
				// // Create directory
				// experimentFolder.mkdirs();
				// // and then create the file
				// logtxt.createNewFile();
				// BufferedWriter buf = new BufferedWriter(new FileWriter(
				// logtxt, true));
				// buf.append(results);
				// buf.newLine();
				// buf.close();
				// }
				//
				// reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (isOnline()) {
				new sendDataToServer().execute();
			} else {
				finalizeExperiment();
				finish();
			}

		}
	}

	public void callGPS() {
		if (gpsEnabled.equalsIgnoreCase("true")) {

		}
	}

	/**
	 * Frame 1-n
	 * 
	 * ViewFrame is a method that renders the frame and calls the 2 renderring
	 * methods and the setOption method.
	 * 
	 * @param NumOfFrame
	 *            which be reduced by one in order to be ok with the Array
	 */
	public void viewFrame(int NumOfFrame) {

		setOptions(NumOfFrame);
		setFrameText(NumOfFrame);
		setPictures(NumOfFrame);
		System.out.println("Viewing :" + (NumOfFrame));
		System.out.println(System.currentTimeMillis());

		/**
		 * TODO UNDELETE
		 */
		// log = log + "  Viewing :" + NumOfFrame + " at :"
		// + (System.currentTimeMillis() - timeStarted) + "\n";
		try {
			writer.write(NumOfFrame + ":"
					+ (System.currentTimeMillis() - timeStarted) + ",");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Frame 1-n
	 * 
	 * This method specifies if a frame is an option frame.We retrieve the
	 * values from the frameOptionValues array.If it's true then we have to go
	 * and take the values of the text for each button and what will happen if
	 * pressed.If not a specific frame is selected then we retrieve from the
	 * array the value "-1" that indicates that the the next frame is specified
	 * by the user in the trial.
	 * 
	 * @param NumOfFrame
	 *            which be reduced by one in order to be ok with the Array
	 */
	public void setOptions(int NumOfFrame) {

		if (frameArray.get(NumOfFrame - 1).frame_type
				.equalsIgnoreCase("optionFrame") == true) {
			bL.setVisibility(View.VISIBLE);
			bR.setVisibility(View.VISIBLE);

			bL.setText(frameArray.get(NumOfFrame - 1).b1_text);
			bR.setText(frameArray.get(NumOfFrame - 1).b2_text);

			if (frameArray.get(NumOfFrame - 1).b1_option
					.equalsIgnoreCase("n/a")) {
				this.nextFrameId1 = -1;
			} else {
				this.nextFrameId1 = Integer.parseInt(frameArray
						.get(NumOfFrame - 1).b1_option);
			}

			if (frameArray.get(NumOfFrame - 1).b2_option
					.equalsIgnoreCase("n/a")) {
				this.nextFrameId2 = -1;
			} else {
				this.nextFrameId2 = Integer.parseInt(frameArray
						.get(NumOfFrame - 1).b2_option);

			}
		} else {
			bL.setVisibility(View.INVISIBLE);
			bR.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Frame 1-n
	 * 
	 * This is a method that so far calls at real time the image to be loaded to
	 * the frame.
	 * 
	 * @param NumOfFrame
	 *            which be reduced by one in order to be ok with the Array
	 */
	// TODO LOAD THE IMAGES TO THE SQLite database prior to the
	// startExperiment();
	public void setPictures(int NumOfFrame) {
		System.out.println("Do we have picture?:"
				+ frameArray.get(NumOfFrame - 1).frame_pic.contains("n/a"));
		if (frameArray.get(NumOfFrame - 1).frame_pic.contains("n/a") != true) {
			//
			// if (ImageMap.get(ImageMap.get(String.valueOf(NumOfFrame))) ==
			// null) {
			// Bitmap pic = BitmapFactory.decodeFile("/mnt/sdcard/Psyapp/Exp"
			// + experimentId + "/" + String.valueOf(NumOfFrame)
			// + ".jpg");
			// ImagePlace.setImageBitmap(pic);
			// } else {
			ImagePlace.setImageBitmap(ImageMap.get(String.valueOf(NumOfFrame)));
			// }

		} else {
			ImagePlace.setImageDrawable(null);
		}

	}

	public void setPicturesforTouch(int NumOfFrame) {
		System.out.println("Do we have picture?:"
				+ frameArray.get(NumOfFrame - 1).frame_pic.contains("n/a"));
		if (frameArray.get(NumOfFrame - 1).frame_pic.contains("n/a") != true) {
			touchSurface
					.setImageBitmap(ImageMap.get(String.valueOf(NumOfFrame)));
		} else {
			touchSurface.setImageDrawable(null);
		}

	}

	public void setFreeTextFrame(int NumOfFrame) {
		setContentView(R.layout.freetext);
		submitFreeInput = (Button) findViewById(R.id.submitFree);
		QuestFreeText = (TextView) findViewById(R.id.QuestFreeText);
		editFreeText = (EditText) findViewById(R.id.editFreeText);
		submitFreeInput.setOnClickListener(this);

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			QuestFreeText.setText(frameArray.get(NumOfFrame - 1).frame_text
					.toString());
		} else {
			QuestFreeText.setText(" ");
		}

		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editFreeText, InputMethodManager.SHOW_IMPLICIT);

	}

	public void setCheckBoxFrame(int NumOfFrame) {
		setContentView(R.layout.checkboxlayout);
		checkBoxPlace = (TableLayout) findViewById(R.id.checkBoxPlace);
		submitCheckBox = (Button) findViewById(R.id.submitCheckBox);
		QuestionCheckBox = (TextView) findViewById(R.id.checkBoxQuestion);
		submitCheckBox.setOnClickListener(this);

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			QuestionCheckBox.setText(frameArray.get(NumOfFrame - 1).frame_text
					.toString());
		} else {
			QuestionCheckBox.setText(" ");
		}

		this.checkid = 0;
		for (String value : frameArray.get(NumOfFrame - 1).values) {
			if ((value != "") && (value != null)) {
				tr = new TableRow(this);
				chk = new CheckBox(this);
				chk.setId(checkid);
				chk.setText(value);
				tr.addView(chk);
				checkBoxPlace.addView(tr);
				this.checkid++;
			}

		}

	}

	public void setRadioBoxFrame(int NumOfFrame) {

		setContentView(R.layout.radioboxlayout);

		submitRadioButton = (Button) findViewById(R.id.submitRadioBox);

		QuestionRadioButton = (TextView) findViewById(R.id.QuestionRadioBox);

		radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);

		submitRadioButton.setOnClickListener(this);

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			QuestionRadioButton
					.setText(frameArray.get(NumOfFrame - 1).frame_text
							.toString());
		} else {
			QuestionRadioButton.setText(" ");
		}

		this.checkid = 0;
		for (String value : frameArray.get(NumOfFrame - 1).values) {
			if ((value != "") && (value != null)) {

				rdb = new RadioButton(this);
				rdb.setId(checkid);
				rdb.setText(value);
				radioGroup.addView(rdb);
				this.checkid++;
			}

		}

	}

	public void setSpinnerFrame(int NumOfFrame) {

		setContentView(R.layout.spinnerframe);

		this.submitSpinner = (Button) findViewById(R.id.submitSpinner);
		this.QuestionSpinner = (TextView) findViewById(R.id.QuestionSpinner);
		this.spinner = (Spinner) findViewById(R.id.spinner);

		submitSpinner.setOnClickListener(this);

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			QuestionSpinner.setText(frameArray.get(NumOfFrame - 1).frame_text
					.toString());
		} else {
			QuestionSpinner.setText(" ");
		}

		System.out.println(frameArray.get(NumOfFrame - 1).values.length);
		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item,
				frameArray.get(NumOfFrame - 1).values);
		spinnerArrayAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(spinnerArrayAdapter);

	}

	public void setFrameText(int NumOfFrame) {

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			TextPlace.setVisibility(View.VISIBLE);
			TextPlace.setText(frameArray.get(NumOfFrame - 1).frame_text
					.toString());
		} else {
			TextPlace.setVisibility(View.INVISIBLE);
		}

	}

	// /**
	// * This frames will create a cameraFrame
	// *
	// * @param NumOfFrame
	// */
	// public void setCameraFrame(int NumOfFrame) {
	// setContentView(R.layout.cameraframe);
	// cameraImageView = (ImageView) findViewById(R.id.cameraImageView);
	// }

	/**
	 * Method to set the seekBarFrame
	 * 
	 * @param NumFrame
	 */
	public void setSeekBarFrame(int NumOfFrame) {
		setContentView(R.layout.seekingbarframe);
		seekBar = (SeekBar) findViewById(R.id.SeekBar);
		submitSeekBar = (Button) findViewById(R.id.submitSeekBar);
		seekBarText = (TextView) findViewById(R.id.QuestionSeekBar);
		submitSeekBar.setOnClickListener(this);
		minSeek = (TextView) findViewById(R.id.min);
		maxSeek = (TextView) findViewById(R.id.max);

		seekBar.setProgress(Integer.parseInt(frameArray.get(NumOfFrame - 1).frame_seekInit));
		// seekBar.incrementProgressBy(10);
		seekBar.setMax(Integer.parseInt(frameArray.get(NumOfFrame - 1).frame_seekMax));

		seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				ExperimentView.seekBarValueString = String.valueOf(progress);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {

			}
		});

		if (frameArray.get(NumOfFrame - 1).frame_text.equalsIgnoreCase("n/a") == false) {
			seekBarText.setText(frameArray.get(NumOfFrame - 1).frame_text
					.toString());
		} else {
			seekBarText.setText(" ");
		}

		System.out.println("Label1 null:"
				+ frameArray.get(NumOfFrame - 1).frame_label1);
		System.out.println("Label2 null:"
				+ frameArray.get(NumOfFrame - 1).frame_label2);
		maxSeek.setText(frameArray.get(NumOfFrame - 1).frame_label1.toString());
		minSeek.setText(frameArray.get(NumOfFrame - 1).frame_label2.toString());

		try {
			writer.write(NumOfFrame + ":"
					+ (System.currentTimeMillis() - timeStarted) + ",");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Frame 1-n
	 * 
	 * This method is the method that will put the thread to sleep in order to
	 * have the Rolling frame Effect.The frameTimeValues Arraylist is an
	 * ArrayList that carries the time specified from the user.If value is not
	 * specified then we set the default sleep time to 3000
	 * 
	 * @param NumOfFrame
	 *            which be reduced by one in order to be ok with the Array
	 */
	public void setFrameTimeLength(int NumOfFrame) {

		try {

			Thread.sleep(Integer.parseInt(frameArray.get(NumOfFrame - 1).frame_timeLength));

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setNormalOptionFrame() {
		setContentView(R.layout.experiment);
		TextPlace = (TextView) findViewById(R.id.textPlace);
		ImagePlace = (ImageView) findViewById(R.id.imagePlace);
		bL = (Button) findViewById(R.id.bL);
		bR = (Button) findViewById(R.id.bR);
		bL.setOnClickListener(this);
		bR.setOnClickListener(this);
		TextPlace.setOnTouchListener(this);
		ImagePlace.setOnClickListener(this);
		ImagePlace.setOnTouchListener(this);
	}

	public void setTouchStuff(int NumOfFrame) {
		this.isItTouchFrame = true;
		this.framePresses = Integer
				.parseInt(frameArray.get(NumOfFrame - 1).frame_presses);

		// DOULEIA TS NUXTAS
		setContentView(R.layout.touchframe);
		touchSurface = (ImageView) findViewById(R.id.touchSurface);
		touchSurface.setOnTouchListener(this);

	}

	public void stopStuff(int NumOfFrame) {
		this.isItTouchFrame = false;
		this.currentPresses = 1;
		this.framePresses = 0;
		if ((NumOfFrame + 1) < frameArray.size()) {
			if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("normalFrame")
					|| (frameArray.get(NumOfFrame + 1).frame_type
							.equalsIgnoreCase("optionFrame"))) {

				setContentView(R.layout.experiment);
				TextPlace = (TextView) findViewById(R.id.textPlace);
				ImagePlace = (ImageView) findViewById(R.id.imagePlace);
				bL = (Button) findViewById(R.id.bL);
				bR = (Button) findViewById(R.id.bR);
				bL.setOnClickListener(this);
				bR.setOnClickListener(this);
				TextPlace.setOnTouchListener(this);
				ImagePlace.setOnClickListener(this);
				ImagePlace.setOnTouchListener(this);

			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("touchFrame")) {
				setContentView(R.layout.touchframe);
				touchSurface = (ImageView) findViewById(R.id.touchSurface);
				touchSurface.setOnTouchListener(this);
			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("freeTextFrame")) {
				/**
				 * NA BALW GIA TO FREETEXT
				 */
				setContentView(R.layout.freetext);
				submitFreeInput = (Button) findViewById(R.id.submitFree);
				QuestFreeText = (TextView) findViewById(R.id.QuestFreeText);
				editFreeText = (EditText) findViewById(R.id.editFreeText);
				submitFreeInput.setOnClickListener(this);
			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("seekBarFrame")) {
				setContentView(R.layout.seekingbarframe);
				seekBar = (SeekBar) findViewById(R.id.SeekBar);
				submitSeekBar = (Button) findViewById(R.id.submitSeekBar);
				seekBarText = (TextView) findViewById(R.id.QuestionSeekBar);
			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("checkBoxFrame")) {

				setContentView(R.layout.checkboxlayout);
				checkBoxPlace = (TableLayout) findViewById(R.id.checkBoxPlace);
				submitCheckBox = (Button) findViewById(R.id.submitCheckBox);
				QuestionCheckBox = (TextView) findViewById(R.id.checkBoxQuestion);

			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("radioBoxFrame")) {

				setContentView(R.layout.radioboxlayout);
				submitRadioButton = (Button) findViewById(R.id.submitRadioBox);
				QuestionRadioButton = (TextView) findViewById(R.id.QuestionRadioBox);
				radioGroup = (RadioGroup) findViewById(R.id.RadioGroup);
			} else if (frameArray.get(NumOfFrame + 1).frame_type
					.equalsIgnoreCase("radioBoxFrame")) {
				setContentView(R.layout.spinnerframe);
				submitSpinner = (Button) findViewById(R.id.submitSpinner);
				QuestionSpinner = (TextView) findViewById(R.id.QuestionSpinner);
				spinner = (Spinner) findViewById(R.id.spinner);

			}
		} else {
			System.out.print("BOOM");
		}
	}

	private Bitmap ImageOperations(String url, String saveFilename) {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			InputStream is = (InputStream) this.fetch(url);
			Bitmap theImage = BitmapFactory.decodeStream(is);
			if (theImage.getHeight() >= 700 || theImage.getWidth() >= 700) {
				theImage = Bitmap.createScaledBitmap(theImage,
						display.getWidth(), display.getHeight() - 140, true);

			}
			/**
			 * AUTH H PATSABOURA MOU PE NA TO BALW
			 */

			return theImage;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	public Object fetch(String address) throws MalformedURLException,
			IOException {
		URL url = new URL(address);
		Object content = url.getContent();
		return content;
	}

	/**
	 * Specifies that when the activity is finished we should clear all the
	 * arrays otherwise the new experiment would be messed up
	 */

	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		frameArray.clear();
		log = "";
		timeStarted = 0;
	}

	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (t1 != null && timeStarted > 0) {
			if (t1.getState() == State.TIMED_WAITING) {

				this.paused = true;
				System.out.println("Egine pause");

			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		System.out.println("Resume");
		if (t1 != null && timeStarted > 0) {
			System.out.println("Eleutherwsa?");
			Toast.makeText(this, "ONRESUME", Toast.LENGTH_SHORT).show();
			if (this.paused == true) {
				System.out.println("Eleutherwsa");
				this.paused = false;
				signalToContinue();
			}
		}

	}

	/**
	 * Synchronized method that make the thread to stop till notify is called
	 */
	public void await() {
		synchronized (this.t1) {
			try {
				this.t1.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Synchronized method that notifies the thread to continue its execution
	 */
	public void signalToContinue() {
		synchronized (this.t1) {
			this.t1.notify();
		}
	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	/**
	 * Method called in order the experiment to start
	 */
	public void startExperiment() {

		setContentView(R.layout.experiment);

		TextPlace = (TextView) findViewById(R.id.textPlace);
		ImagePlace = (ImageView) findViewById(R.id.imagePlace);
		bL = (Button) findViewById(R.id.bL);
		bR = (Button) findViewById(R.id.bR);
		bL.setOnClickListener(this);
		bR.setOnClickListener(this);
		TextPlace.setOnTouchListener(this);
		ImagePlace.setOnClickListener(this);
		ImagePlace.setOnTouchListener(this);

		final FrameRoll frameroll = new FrameRoll();
		this.t1 = new Thread() {
			public void run() {
				frameroll.run();
			}
		};
		this.t1.start();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		Toast.makeText(this, "Got Result " + String.valueOf(resultCode),
				Toast.LENGTH_SHORT).show();

		switch (requestCode) {
		case GPSHANDLER_ACTIVITY_RETURN:
			if (resultCode == Activity.RESULT_OK) {

				String location = data.getStringExtra("location");
				log = log + location;

				if (timeStarted != 0) {
					signalToContinue();
				}

			} else {
				if (data.getStringExtra("Reason") != null) {
					String reason = data.getStringExtra("Reason");
					log = log + "Location not retrieved:reason : " + reason;
				} else {
					log = log + "Location not retrieved";
				}
				if (timeStarted != 0) {
					signalToContinue();
				}
			}

			break;
		case CAMERA_INTENT_ID:
			if (resultCode == Activity.RESULT_OK) {
				Uri selectedImage = imageUri;
				getContentResolver().notifyChange(selectedImage, null);
				ImageView imageView = (ImageView) findViewById(R.id.cameraImageView);
				ContentResolver cr = getContentResolver();
				Bitmap bitmap;
				try {
					bitmap = android.provider.MediaStore.Images.Media
							.getBitmap(cr, selectedImage);

					imageView.setImageBitmap(bitmap);
					Toast.makeText(this, selectedImage.toString(),
							Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
							.show();
					Log.e("Camera", e.toString());
				}
			}

			break;

		default:
			break;
		}

	}

	public boolean onTouch(View v, MotionEvent event) {

		if (v.getId() == R.id.touchSurface) {
			if (this.isItTouchFrame == true) {
				if (this.currentPresses < this.framePresses) {

					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						System.out.println("Touched at X:" + event.getRawX()
								+ " Y:" + event.getRawY());
						try {
							writer.write("Touched at X:"
									+ event.getRawX()
									+ " Y:"
									+ event.getRawY()
									+ " and pressure :"
									+ event.getPressure()
									+ " at :"
									+ (System.currentTimeMillis() - timeStarted)
									+ ",");
							
							JsonObject jsonFrame = (JsonObject) frameLogArray
									.get(frameLogArray.size() - 1);
							if (jsonFrame.get("data") == null) {
								JsonArray data = new JsonArray();
								JsonObject datasJson = new JsonObject();
								datasJson.addProperty("x", event.getX());
								datasJson.addProperty("y", event.getY());
								datasJson.addProperty("press",
										event.getPressure());
								datasJson.addProperty("dtm",
										System.currentTimeMillis()
												- timeStarted);
								data.add(datasJson);
								jsonFrame.add("data", data);

							} else {
								JsonObject datasJson = new JsonObject();
								datasJson.addProperty("x", event.getX());
								datasJson.addProperty("y", event.getY());
								datasJson.addProperty("press",
										event.getPressure());
								datasJson.addProperty("dtm",
										System.currentTimeMillis()
												- timeStarted);
								JsonArray data = (JsonArray) jsonFrame
										.get("data");
								data.add(datasJson);
							}

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						//

						this.currentPresses++;
					}

				} else if (this.currentPresses == this.framePresses) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						System.out.println("Touched at X:" + event.getRawX()
								+ " Y:" + event.getRawY());
						try {
							writer.write("Touched at X:"
									+ event.getRawX()
									+ " Y:"
									+ event.getRawY()
									+ " and pressure :"
									+ event.getPressure()
									+ " at :"
									+ (System.currentTimeMillis() - timeStarted)
									+ ",");
							JsonObject jsonFrame = (JsonObject) frameLogArray
									.get(frameLogArray.size() - 1);

							if (jsonFrame.get("data") == null) {
								JsonArray data = new JsonArray();
								JsonObject datasJson = new JsonObject();
								datasJson.addProperty("x", event.getX());
								datasJson.addProperty("y", event.getY());
								datasJson.addProperty("press",
										event.getPressure());
								datasJson.addProperty("dtm",
										System.currentTimeMillis()
												- timeStarted);
								data.add(datasJson);
								jsonFrame.add("data", data);

							} else {
								JsonObject datasJson = new JsonObject();
								datasJson.addProperty("x", event.getX());
								datasJson.addProperty("y", event.getY());
								datasJson.addProperty("press",
										event.getPressure());
								datasJson.addProperty("dtm",
										System.currentTimeMillis()
												- timeStarted);
								JsonArray data = (JsonArray) jsonFrame
										.get("data");
								data.add(datasJson);
							}

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						this.isItTouchFrame = false;
						this.currentPresses = 1;
						signalToContinue();
					}
				}
			}

		}

		return false;
	}

	/**
	 * We take everything from the log file and transfer them in the log
	 * variable.Then make sure the experiment is saved in the db
	 */
	public void finalizeExperiment() {

		// Setting up notification
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,
				"Psyapp trying to sync..", System.currentTimeMillis());
		notification.setLatestEventInfo(getApplicationContext(),
				"Psyapp trying to sync", "Trying to sync experiment"
						+ experimentId, PendingIntent.getActivity(
						ExperimentView.this, 0, new Intent(ExperimentView.this,
								MainPsyAppActivity.class),
						PendingIntent.FLAG_CANCEL_CURRENT));

		mNotificationManager.notify(NOTIFICATION_ID, notification);

		DatabaseHandler db = new DatabaseHandler(ExperimentView.this);
		ExperimentResponse response = new ExperimentResponse(experimentId, log,
				"false");

		db.addResponse(response);

		Intent uploadService = new Intent(ExperimentView.this,
				UploadService.class);
		uploadService.putExtra("response", response);
		startService(uploadService);

	}

	public class sendDataToServer extends AsyncTask<String, Integer, String> {
		private ProgressDialog progreDialog;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			runOnUiThread(new Runnable() {

				public void run() {
					ImageMap.clear();
					progreDialog = new ProgressDialog(ExperimentView.this);
					progreDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					progreDialog
							.setMessage("Uploading results to the server..");
					progreDialog.show();
					Log.i("onPreExecute", "mphke");

				}
			});

		}

		protected String doInBackground(String... params) {

			System.out.println("Is online?:" + isOnline());
			HttpRequestsHandler post = new HttpRequestsHandler();
			System.out.println("JSON " + frameLogArray);
			post.postData(experimentId, log);

			// Delete the alarm/experiment from the database if
			// we have passed such thing
			if (UID_OF_ALARM_FOR_NOT != null) {
				DatabaseHandler db = new DatabaseHandler(
						getApplicationContext());
				db.deleteAlarm(UID_OF_ALARM_FOR_NOT);
				System.out.println("Trying to delete UID:"
						+ UID_OF_ALARM_FOR_NOT);
				try {
					String ns = Context.NOTIFICATION_SERVICE;
					NotificationManager nMgr = (NotificationManager) getApplicationContext()
							.getSystemService(ns);
					nMgr.cancel(Integer.parseInt(UID_OF_ALARM_FOR_NOT
							.substring(1, 7)));
				} catch (Exception e) {

				}
			}

			// // Reading all contacts
			// Log.d("Reading: ", "Reading all contacts..");
			// List<ExperimentResponse> contacts = db.getAllContacts();
			//
			// for (ExperimentResponse exp : contacts) {
			// String log = "Id: " + exp.get_id() + " ,Name: "
			// + exp.get_exp_id() + " ,Phone: " + exp.get_response();
			// // Writing Contacts to log
			// Log.d("Name: ", log);
			// }
			// db.close();

			return "RESULT_OK";
		}

		protected void onPostExecute(String result) {

			runOnUiThread(new Runnable() {

				public void run() {
					progreDialog.dismiss();
					finish();

				}
			});

		}
	}

	public class setUptheExperiment extends AsyncTask<String, Integer, String> {
		private ProgressDialog progreDialog;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			runOnUiThread(new Runnable() {

				public void run() {
					progreDialog0 = new ProgressDialog(ExperimentView.this);
					progreDialog0
							.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					progreDialog0
							.setMessage("Downloading stimuli from the net");
					progreDialog0.setMax(10);
					progreDialog0.show();

				}
			});

		}

		protected String doInBackground(String... params) {
			int frameNum = 1;
			String preparedTitle;

			File experimentFolder = new File(
					Environment.getExternalStorageDirectory() + "/Psyapp",
					"Exp" + experimentId);
			if (experimentFolder.exists()) {
				for (Frame frame : frameArray) {
					System.out.println(frame.frame_pic);
					if (frame.frame_pic.contentEquals("true") == true) {

						File imgFile = new File(experimentFolder + "/"
								+ frameNum + ".jpg");
						Log.i(TAG, "Loading " + imgFile.getName() + " of frame"
								+ frameNum);
						Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
								.getAbsolutePath());

						ImageMap.put(String.valueOf(frameNum), myBitmap);

					}
					frameNum++;
				}
			} else {
				experimentFolder.mkdirs();
				for (Frame frame : frameArray) {
					Bitmap theImage;
					System.out.println(frame.frame_pic);
					if (frame.frame_pic.contentEquals("true") == true) {

						String url = "http://psyapp.co.uk/application/showexperimentspics?id="
								+ experimentId
								+ "&idFrame="
								+ String.valueOf(frameNum);

						// String url =
						// "http://192.168.1.11:9003/application/showexperimentspics?id="
						// + experimentId
						// + "&idFrame="
						// + String.valueOf(frameNum);

						try {
							Display display = getWindowManager()
									.getDefaultDisplay();
							InputStream is = (InputStream) fetch(url);
							theImage = BitmapFactory.decodeStream(is);
							if (theImage.getHeight() >= 700
									|| theImage.getWidth() >= 700) {
								theImage = Bitmap.createScaledBitmap(theImage,
										display.getWidth(),
										display.getHeight() - 140, true);
							}
							/**
							 * AUTH H PATSABOURA MOU PE NA TO BALW
							 */

						} catch (MalformedURLException e) {
							e.printStackTrace();
							return null;
						} catch (IOException e) {
							e.printStackTrace();
							return null;
						}
						preparedTitle = frameNum + ".jpg";

						OutputStream fOut = null;
						Log.i(TAG,
								"Trying to save image to:"
										+ Environment
												.getExternalStorageDirectory()
										+ "/Psyapp");
						File file = new File(
								Environment.getExternalStorageDirectory()
										+ "/Psyapp/Exp" + experimentId,
								preparedTitle);

						try {

							fOut = new FileOutputStream(file);
							theImage.compress(Bitmap.CompressFormat.JPEG, 76,
									fOut);
							fOut.flush();
							fOut.close();
							// MediaStore.Images.Media.insertImage(
							// getContentResolver(),
							// file.getAbsolutePath(), file.getName(),
							// file.getName());
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						Log.i(TAG, "saving in the Hashmap");
						ImageMap.put(String.valueOf(frameNum), theImage);

					}
					frameNum++;
				}
			}
			return "RESULT_OK";
		}

		protected void onPostExecute(String result) {

			runOnUiThread(new Runnable() {

				public void run() {
					progreDialog0.dismiss();

				}
			});

		}
	}
}
