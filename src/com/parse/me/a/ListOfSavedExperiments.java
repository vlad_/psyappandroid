package com.parse.me.a;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.parse.me.a.Models.Experiment;
import com.parse.me.a.Models.Frame;
import com.parse.me.a.Models.Trial;
import com.parse.me.database.DatabaseHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ListOfSavedExperiments extends Activity implements
		OnItemClickListener {

	private static final String TAG = "ListOfSavedExperiments";
	DatabaseHandler db = new DatabaseHandler(this);
	private SimpleCursorAdapter dataAdapter;
	TextView tv;
	ListView listView;
	public int selectedExperiment;
	public String experimentId;
	public String gpsEnabled;
	public ArrayList<ArrayList<String>> listOftrials;
	private ArrayList<Frame> frameArray;
	public Cursor currentCursor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listresponse);
		displayListView();
		listOftrials = new ArrayList<ArrayList<String>>();
		frameArray = new ArrayList<Frame>();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		this.listOftrials.clear();
		this.frameArray.clear();
	}

	private void displayListView() {

		Cursor cursor = db.getAllExperimentCursor();
		// The desired columns to be bound
		String[] columns = new String[] { DatabaseHandler.getIdOfExp() };
		// the XML defined views which the data will be bound to
		int[] to = new int[] { R.id.code };
		// create the adapter using the cursor pointing to the desired data
		// as well as the layout information
		dataAdapter = new SimpleCursorAdapter(this, R.layout.response_info,
				cursor, columns, to);
		listView = (ListView) findViewById(R.id.listView1);
		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);
		listView.setOnItemClickListener(this);

	}

	public void onItemClick(AdapterView<?> adapter, View view, int position,
			long id) {
		// TODO Auto-generated method s
		switch (position) {
		case 100:
			Log.i(TAG, "pressed");
			break;

		default:
			this.currentCursor = ((SimpleCursorAdapter) ((ListView) listView)
					.getAdapter()).getCursor();
			this.selectedExperiment = Integer.parseInt(this.currentCursor
					.getString(1));
			if (isOnline()) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(
						this);
				builder.setMessage(
						"You are about to run experiment "
								+ this.currentCursor.getString(1) + ".Proceed?")
						.setCancelable(false)
						.setPositiveButton("Sure",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										new ConvertToExperiment().execute();
									}

								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();

									}
								});
				final AlertDialog alert = builder.create();
				alert.show();
			} else {

				final AlertDialog.Builder builder = new AlertDialog.Builder(
						this);
				builder.setMessage(
						"Experiment will be started but we see that you are not connected to the internet.Please connect in order to continue")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										Intent settingsMenu = new Intent(
												Settings.ACTION_WIRELESS_SETTINGS);
										startActivityForResult(settingsMenu, 1);

									}

								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
										Intent resultIntent = new Intent();
										resultIntent
												.putExtra("Reason",
														"wifi off.Refused to turn it on");
										setResult(Activity.RESULT_CANCELED,
												resultIntent);
										finish();

									}
								});
				final AlertDialog alert = builder.create();
				alert.show();
			}

			break;
		}
	}

	private void prepareParcelandSend() {
		// TODO Auto-generated method stub
		Bundle parameters = new Bundle();

		parameters.putString("exp_id", this.experimentId);
		parameters.putString("gpsEnabled", this.gpsEnabled);
		Intent m2Exp = new Intent(ListOfSavedExperiments.this,
				ExperimentView.class);
		m2Exp.putExtras(parameters);
		m2Exp.putExtra("Frames", frameArray);
		m2Exp.putExtra("Trials", listOftrials);
		startActivity(m2Exp);

	}

	public class ConvertToExperiment extends
			AsyncTask<String, AsyncTask, String> {

		ProgressDialog progDialog;
		private String result;

		/**
		 * Before the request view the progress dialog
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progDialog = new ProgressDialog(ListOfSavedExperiments.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog.setMessage("Setting up experiment");
			progDialog.setMax(10);
			progDialog.show();
			Log.i(TAG, "Start converting");
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			Gson gson = new Gson();
			int frameCounter = 1;
			String log = currentCursor.getString(3);
			Log.i(TAG, log);
			Experiment experiment = gson.fromJson(log, Experiment.class);

			experimentId = experiment.exp_id;
			gpsEnabled = experiment.exp_gpsEnabled;
			ArrayList<String> tempStrings = new ArrayList<String>();
			for (Trial[] arrays : experiment.trials) {
				tempStrings = new ArrayList<String>();
				for (Trial id : arrays) {
					if (id.toString() != null && id.toString() != ""
							&& id.toString() != " ") {
						tempStrings.add(id.toString());
						System.out.println(id.toString());
					}
				}
				System.out.println("other array");
				listOftrials.add(tempStrings);

			}

			// List of the frames inside the object
			List<Frame> frames = experiment.frames;
			for (Frame indyFrame : frames) {

				Frame frame = new Frame(String.valueOf(frameCounter),
						indyFrame.frame_type, indyFrame.frame_text,
						indyFrame.frame_pic, indyFrame.frame_pic);
				if (indyFrame.frame_type.equalsIgnoreCase("normalFrame")) {
					frame.setTime(indyFrame.frame_timeLength);
				} else if (indyFrame.frame_type.equalsIgnoreCase("optionFrame")) {
					frame.setb1(indyFrame.b1_text);
					frame.setb2(indyFrame.b2_text);
					frame.setB1option(indyFrame.b1_option);
					frame.setB2option(indyFrame.b2_option);
				} else if (indyFrame.frame_type.equalsIgnoreCase("touchFrame")) {
					frame.setPresses(indyFrame.frame_presses);
				} else if (indyFrame.frame_type
						.equalsIgnoreCase("freeTextFrame")) {
					frame.frame_pic = "n/a";
				} else if (indyFrame.frame_type
						.equalsIgnoreCase("seekBarFrame")) {
					frame.frame_pic = "n/a";
					frame.setFrame_seekInit(indyFrame.frame_seekInit);
					frame.setFrame_seekMax(indyFrame.frame_seekMax);
					frame.setFrame_label1(indyFrame.frame_label1);
					frame.setFrame_label2(indyFrame.frame_label2);
				} else if (indyFrame.frame_type
						.equalsIgnoreCase("checkBoxFrame")) {
					frame.frame_pic = "n/a";

					int g = 0;
					for (Object value : indyFrame.options) {
						frame.values[g] = value.toString();

						g++;
					}

				} else if (indyFrame.frame_type
						.equalsIgnoreCase("radioBoxFrame")) {
					frame.frame_pic = "n/a";

					int g = 0;
					for (Object value : indyFrame.options) {
						frame.values[g] = value.toString();

						g++;
					}
				} else if (indyFrame.frame_type
						.equalsIgnoreCase("spinnerFrame")) {
					frame.frame_pic = "n/a";
					frame.values = new String[indyFrame.options.size()];
					int g = 0;
					for (Object value : indyFrame.options) {
						frame.values[g] = value.toString();

						g++;
					}

				}else if (indyFrame.frame_type
						.equalsIgnoreCase("cameraFrame")) {
				
					}

				frameCounter++;
				System.out.println("frame_id" + frame.frame_id);
				System.out.println("frame_type" + frame.frame_type);
				System.out.println("frame_text" + frame.frame_text);
				System.out.println("frame_time" + frame.frame_timeLength);

				frameArray.add(frame);
				System.out.println("Frames num :" + frameArray.size());

			}

			this.result = "RESULT_OK";
			return result;
		}

		/**
		 * On the end,if the result is retrieved properly go to the other
		 * activity
		 */
		protected void onPostExecute(String result) {

			progDialog.cancel();
			prepareParcelandSend();

		}

	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

}
