package com.parse.me.a;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class GpsHandler extends Activity {
	// Layout parameters
	String addressString = "No address found";
	TextView myLocationTV;
	TextView optimalTV;
	TextView infoGps;
	LocationManager locationManager;

	// Gps position parameters
	Location optimalLocation;
	LocationListener locationListener;
	public static final int RETURN_FROM_GPS_MENU_ENABLE = 0;
	public static final int RETURN_FROM_GPS_MENU_DISABLE = 1;
	int timesUpdated;
	String providerFine;
	String providerCoarse;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gpshandlerlayout);
		myLocationTV = (TextView) findViewById(R.id.gpsCurrent);
		optimalTV = (TextView) findViewById(R.id.gpsOptimal);
		infoGps = (TextView) findViewById(R.id.gpsinfo);

		// Check if the gps is enabled

		String context = Context.LOCATION_SERVICE;
		locationManager = (LocationManager) getSystemService(context);

		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			buildAlertMessageGps(false);
		} else {
			/**
			 * ENABLE TO HAVE NORMAL GPS AGAIN
			 */
			buildAlertMessageGps(true);
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	private void updateWithLocation(Location location) {

		String latLongString;

		if (location != null) {

			if (isBetterLocation(location, optimalLocation)) {
				optimalLocation = location;
				optimalTV.setText(optimalLocation.getLatitude() + ":"
						+ optimalLocation.getLongitude() + "/n prec:"
						+ optimalLocation.getAccuracy());
			}
			latLongString = "Lat:" + location.getLatitude() + " and \nLong:"
					+ location.getLongitude() + "\nwith precision: "
					+ location.getAccuracy();

		} else {
			latLongString = "No location";
		}

		myLocationTV.setText("My location is :" + latLongString);

	}

	private void setUpGps() {

		locationListener = new LocationListener() {

			public void onLocationChanged(Location location) {
				// Called when a new location is found by the network
				// location provider.

				if (optimalLocation != null) {
					if (optimalLocation.getAccuracy() > 15) {

						updateWithLocation(location);

					} else {
						Intent resultIntent = new Intent();
						resultIntent.putExtra("location",
								optimalLocation.toString());
						setResult(Activity.RESULT_OK, resultIntent);
						finish();
					}
				} else {
					infoGps.setText("Locking position...");
					updateWithLocation(location);

				}

			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
				updateWithLocation(null);
			}

			public void onProviderDisabled(String provider) {
			}
		};

		Criteria criteria = new Criteria();
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);
		criteria.setCostAllowed(false);
		criteria.setPowerRequirement(Criteria.POWER_HIGH);

		// Set up the gps sensor
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		this.providerFine = locationManager.getBestProvider(criteria, true);
		// Set up the wifi sensor
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		this.providerCoarse = locationManager.getBestProvider(criteria, true);

		if (providerFine == null) {
			Location location = locationManager
					.getLastKnownLocation(providerFine);

			updateWithLocation(location);
			optimalLocation = location;
			optimalTV.setText(optimalLocation.getLatitude() + ":"
					+ optimalLocation.getLongitude() + "/n prec:"
					+ optimalLocation.getAccuracy());
		}

		locationManager.requestLocationUpdates(providerFine, 0, 0,
				locationListener);

		locationManager.requestLocationUpdates(providerCoarse, 0, 0,
				locationListener);
		Timer closeGpsPlease = new Timer("GPS");

		closeGpsPlease.schedule(new TimerTask() {

			@Override
			public void run() {

				locationManager.removeUpdates(locationListener);

				if (optimalLocation != null) {
					Intent resultIntent = new Intent();
					resultIntent.putExtra("location",
							optimalLocation.toString());

					setResult(Activity.RESULT_OK, resultIntent);

					finish();
				} else {
					Intent resultIntent = new Intent();
					resultIntent
							.putExtra("Reason", "GPS enabled but no signal");
					setResult(Activity.RESULT_CANCELED, resultIntent);
					finish();
				}
			}
		}, 1000 * 10);

	}

	private void buildAlertMessageGps(boolean gpsEnabled) {

		if (!gpsEnabled) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"PsyApp takes privacy seriously. This experiment requires collection of your GPS position. Please acknowledge you are aware of this and give your consent.Yout GPS seems to be disabled, do you want to enable it?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent settingsMenu = new Intent(
											Settings.ACTION_LOCATION_SOURCE_SETTINGS);
									startActivityForResult(settingsMenu,
											RETURN_FROM_GPS_MENU_ENABLE);

								}

							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									Intent resultIntent = new Intent();
									resultIntent.putExtra("Reason",
											"GPS off.Refused to turn it on");
									setResult(Activity.RESULT_CANCELED,
											resultIntent);
									finish();

								}
							});
			final AlertDialog alert = builder.create();
			alert.show();
		} else {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"PsyApp takes privacy seriously. This experiment requires collection of your GPS position. Please acknowledge you are aware of this and give your consent.")
					.setCancelable(false)
					.setPositiveButton("I do",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									setUpGps();
									dialog.cancel();

								}
							})
					.setNegativeButton("I don't",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									Intent resultIntent = new Intent();
									resultIntent.putExtra("Reason",
											"GPS off.Refused to turn it on");
									setResult(Activity.RESULT_CANCELED,
											resultIntent);
									finish();
								}
							});
			final AlertDialog alert = builder.create();
			alert.show();
			
			setUpGps();

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RETURN_FROM_GPS_MENU_ENABLE && resultCode == 0) {
			String provider = Settings.Secure.getString(getContentResolver(),
					Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (provider != null) {
				Toast.makeText(GpsHandler.this, "GPS enabled",
						Toast.LENGTH_SHORT).show();
				setUpGps();
			} else {
				// Users did not switch on the GPSs
				buildAlertMessageGps(false);
			}
		} else if (requestCode == RETURN_FROM_GPS_MENU_DISABLE
				&& resultCode == 0) {
			Toast.makeText(GpsHandler.this, "GPS disabled", Toast.LENGTH_SHORT)
					.show();

		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/**
	 * Determines whether one Location reading is better than the current
	 * Location fix
	 * 
	 * @param location
	 *            The new Location that you want to evaluate
	 * @param currentBestLocation
	 *            The current Location fix, to which you want to compare the new
	 *            one
	 */
	protected boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			// A new location is always better than no location
			return true;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isLessAccurate || isSignificantlyLessAccurate) {
			return false;
		}
		return false;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (this.providerFine != null) {
			locationManager.removeUpdates(locationListener);
		}
	}
}
