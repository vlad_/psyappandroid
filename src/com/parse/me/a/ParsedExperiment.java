package com.parse.me.a;

import java.util.ArrayList;

import com.parse.me.a.Models.Frame;

public class ParsedExperiment {

	// The experiment to be pulled
	private String idOfExp;
	// Type of parsedExperiment
	private boolean startNow;
	// The response from server used in DB things
	private String serverResponse;
	// Frame Array is going to used in
	private ArrayList<Frame> frameArray;

	public ParsedExperiment(String idOfExp, boolean startNow) {
		this.setIdOfExp(idOfExp);
		this.setStartNow(startNow);
	}

	public ArrayList<Frame> pullExperiment() {
    
		return frameArray;
	}

	public String getIdOfExp() {
		return idOfExp;
	}

	public void setIdOfExp(String idOfExp) {
		this.idOfExp = idOfExp;
	}

	public boolean getStartNow() {
		return startNow;
	}

	public void setStartNow(boolean startNow) {
		this.startNow = startNow;
	}

}
