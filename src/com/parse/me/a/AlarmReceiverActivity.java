package com.parse.me.a;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.parse.me.a.Models.Experiment;
import com.parse.me.a.Models.Frame;
import com.parse.me.a.Models.Trial;
import com.parse.me.database.DatabaseHandler;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class AlarmReceiverActivity extends Activity implements OnClickListener {

	private MediaPlayer mMediaPlayer;
	private String idOfExp;
	private String idOfNot;
	NotificationManager mNotificationManager;
	Button turnOff, later, now;
	TextView message;
	DatabaseHandler db = new DatabaseHandler(this);

	public int selectedExperiment;
	public String experimentId;
	public String gpsEnabled;
	public ArrayList<ArrayList<String>> listOftrials;
	private ArrayList<Frame> frameArray;

	private String TAG = "AlarmReceiver";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle params = getIntent().getExtras();
		idOfExp = params.getString("id");
		System.out.println("ID OF EXP" + getIdOfExp());
		setIdOfExp(idOfExp);
		this.idOfNot = idOfExp;
		System.out.println("ID :" + getIdOfExp());
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.alarmcontroller);
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		message = (TextView) findViewById(R.id.message);
		listOftrials = new ArrayList<ArrayList<String>>();
		frameArray = new ArrayList<Frame>();
		turnOff = (Button) findViewById(R.id.disable);
		later = (Button) findViewById(R.id.later);
		now = (Button) findViewById(R.id.now);
		turnOff.setOnClickListener(this);
		later.setOnClickListener(this);
		now.setOnClickListener(this);
		message.setText("It's time for experiment " + idOfExp);

		playSound(this, getAlarmUri());

	}

	private void setNotification(String idOfExp, String notId) {

		// TODO Auto-generated method stub
		Notification notification = new Notification(R.drawable.ic_launcher,
				"Psyapp alarm system", System.currentTimeMillis());
		Intent newIntent = new Intent(getApplicationContext(),
				MainPsyAppActivity.class);
		newIntent.putExtra("ExpId", String.valueOf(idOfExp));
		newIntent.putExtra("UID", notId);
		newIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notification.setLatestEventInfo(getApplicationContext(), "Experiment"
				+ idOfExp + " is waiting for you",
				"Click here to disable the alarm clock", PendingIntent
						.getActivity(getApplicationContext(),
								Integer.valueOf(idOfExp), newIntent,
								PendingIntent.FLAG_UPDATE_CURRENT));
		mNotificationManager.notify(Integer.parseInt(notId.substring(1, 7)),
				notification);
		System.out.println("UID2:" + notId);
	}

	private void playSound(Context context, Uri alert) {
		mMediaPlayer = new MediaPlayer();
		try {
			mMediaPlayer.setDataSource(context, alert);
			final AudioManager audioManager = (AudioManager) context
					.getSystemService(Context.AUDIO_SERVICE);
			if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
				mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
				mMediaPlayer.prepare();
				mMediaPlayer.start();
			}
		} catch (IOException e) {
			System.out.println("OOPS");
		}
	}

	// Get an alarm sound. Try for an alarm. If none set, try notification,
	// Otherwise, ringtone.
	private Uri getAlarmUri() {
		Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		if (alert == null) {
			alert = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			if (alert == null) {
				alert = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
			}
		}
		return alert;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		mMediaPlayer.stop();
		setNotification(idOfExp, "0");
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		mMediaPlayer.stop();
		setNotification(idOfExp, "0");
		finish();
		return false;

	}

	public void onClick(View arg0) {
		String uid_for_alarm_AND_not = "";
		DatabaseHandler db = new DatabaseHandler(this);
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.later:
			// disable the alarm
			mMediaPlayer.stop();
			// set notification number
			uid_for_alarm_AND_not = idOfExp + String.valueOf(System.nanoTime());
			setNotification(idOfExp, uid_for_alarm_AND_not);
			// and save to the db
			System.out.println("TEST FINAL:" + uid_for_alarm_AND_not);
			db.addAlarmToBePulled(idOfExp, uid_for_alarm_AND_not);
			db.getAllAlarms();
			finish();
			break;
		case R.id.now:

			// disable sound
			mMediaPlayer.stop();

			uid_for_alarm_AND_not = idOfExp + String.valueOf(System.nanoTime());

			// and save to the db
			db = new DatabaseHandler(this);
			db.addAlarmToBePulled(idOfExp, uid_for_alarm_AND_not);

			Intent hereToMain = new Intent(this, MainPsyAppActivity.class);
			hereToMain.putExtra("ExpId", idOfExp);
			hereToMain.putExtra("UID", uid_for_alarm_AND_not);
			hereToMain.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(hereToMain);
			finish();

			break;
		case R.id.disable:
			mMediaPlayer.stop();
			break;
		default:
			break;
		}
	}

	public String getIdOfExp() {
		return idOfExp;
	}

	public void setIdOfExp(String idOfExp) {
		this.idOfExp = idOfExp;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

	}

}
