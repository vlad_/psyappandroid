package com.parse.me.a.Models;

import java.io.Serializable;

public class StoredExperiment implements Serializable {

	private int _id;
	private String exp_id;
	private boolean gps_enabled;
	private String log;
	private boolean repeatable;

	public StoredExperiment(String exp_id, boolean gps_enabled, String log,
			boolean repeatable) {
		this.exp_id = exp_id;
		this.gps_enabled = gps_enabled;
		this.log = log;
		this.repeatable = repeatable;

	}

	public String getExp_id() {
		return exp_id;
	}

	public void setExp_id(String exp_id) {
		this.exp_id = exp_id;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public boolean isGps_enabled() {
		return gps_enabled;
	}

	public void setGps_enabled(boolean gps_enabled) {
		this.gps_enabled = gps_enabled;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public boolean isRepeatable() {
		return repeatable;
	}

	public void setRepeatable(boolean repeatable) {
		this.repeatable = repeatable;
	}

}
