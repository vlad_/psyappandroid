package com.parse.me.a.Models;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 *
 */
public class Trial {

	@SerializedName(value = "f_id")
	public String id;
	
	public List<String> ids = new ArrayList<String>(); 
	
	
	@Override
	  public String toString()
	  {
	    return String.format(id);
	  }

}
