package com.parse.me.a.Models;

import java.io.Serializable;
import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 * 
 */
public class Frame implements Serializable {

	@SerializedName("frame_id")
	public String frame_id;

	@SerializedName("frame_text")
	public String frame_text;

	@SerializedName("frame_picture")
	public String frame_pic;

	@SerializedName("frame_sound")
	public String frame_sound;

	@SerializedName("frame_timeLength")
	public String frame_timeLength;

	@SerializedName("frame_option")
	public String frame_option;

	@SerializedName("b1_text")
	public String b1_text;

	@SerializedName("b2_text")
	public String b2_text;

	@SerializedName("b1_option")
	public String b1_option;

	@SerializedName("b2_option")
	public String b2_option;

	@SerializedName("frame_type")
	public String frame_type;

	@SerializedName("frame_presses")
	public String frame_presses;

	@SerializedName("frame_seekInit")
	public String frame_seekInit;

	@SerializedName("frame_seekMax")
	public String frame_seekMax;
	
	@SerializedName("frame_label1")
	public String frame_label1;

	@SerializedName("frame_label2")
	public String frame_label2;
	
    public ArrayList<OptionValue> options;
    
    public String[] values = new String[5];
    

    
	public Frame(String idFrame, String type, String text, String picture,
			String sound) {
		this.frame_id = idFrame;
		this.frame_type = type;
		this.frame_text = text;
		this.frame_pic = picture;
		this.frame_sound = sound;
		
	}

	public void setTime(String time) {
		this.frame_timeLength = time;
	}

	public void setb1(String option) {
		this.b1_text = option;
	}

	public void setb2(String option) {
		this.b2_text = option;
	}

	public void setB1option(String b1option) {
		this.b1_option = b1option;
	}

	public void setB2option(String b2option) {
		this.b2_option = b2option;
	}

	public void setPresses(String times) {
		this.frame_presses = times;
	}

	public String getFrame_presses() {
		return frame_presses;
	}

	public void setFrame_presses(String frame_presses) {
		this.frame_presses = frame_presses;
	}

	public void setFrame_seekInit(String frame_seekInit) {
		this.frame_seekInit = frame_seekInit;
	}


	public void setFrame_seekMax(String frame_seekMax) {
		this.frame_seekMax = frame_seekMax;
	}
	
	public void setFrame_label1(String frame_label1) {
		this.frame_label1 = frame_label1;
	}

	public void setFrame_label2(String frame_label2) {
		this.frame_label2 = frame_label2;
	}

	
	



}
