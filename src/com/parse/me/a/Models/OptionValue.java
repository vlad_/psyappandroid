package com.parse.me.a.Models;

import com.google.gson.annotations.SerializedName;

public class OptionValue {

	@SerializedName(value = "optionText")
	public String option;
	
	@Override
	  public String toString()
	  {
	    return String.format(option);
	  }
}
