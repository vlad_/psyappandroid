package com.parse.me.a.Models;

import java.io.Serializable;
import java.util.ArrayList;
import com.google.gson.annotations.SerializedName;

public class Alarm implements Serializable {

	@SerializedName("date")
	public String date;

	@SerializedName("alarmClockType")
	public String alarmClockType;

	@SerializedName("type")
	public String type;

	@SerializedName("nextExp")
	public String nextExp;

	public Alarm(String date) {
		this.date = date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setAlarmClockType(String time) {
		this.alarmClockType = time;
	}

	public void setType(String type) {
		this.type = type;
	}

}
