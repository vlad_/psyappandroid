package com.parse.me.a.Models;

import java.io.Serializable;

public class ExperimentResponse implements Serializable{

	// private variables
	private int _id;
	private String _exp_id;
	private String _response;
	private String _synced;
	
	public ExperimentResponse(){
		
	}
	// Constructor for the experiment
	public ExperimentResponse( String exp_di, String response ,String synced) {
		this._exp_id = exp_di;
		this._response = response;
		this._synced=synced;
	}
	
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_exp_id() {
		return _exp_id;
	}

	public void set_exp_id(String _exp_id) {
		this._exp_id = _exp_id;
	}

	public String get_response() {
		return _response;
	}

	public void set_response(String _response) {
		this._response = _response;
	}
	public String get_synced() {
		return _synced;
	}
	public void set_synced(String _synced) {
		this._synced = _synced;
	}


}

