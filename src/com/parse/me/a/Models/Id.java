package com.parse.me.a.Models;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 *
 */
public class Id {

	@SerializedName(value = "f_id")
	public String id;
}
