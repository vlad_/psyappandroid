package com.parse.me.a.Models;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 *
 */
public class Experiment {

	@SerializedName("experiment_id")
	public String exp_id;
	@SerializedName("experiment_title")
	public String exp_tile;
	@SerializedName("experiment_prive")
	public boolean exp_prive;
	@SerializedName("experiment_gpsEnabled")
	public String exp_gpsEnabled;

	public Trial[][] trials;
	
	public List<Frame> frames;
	
	public List<Alarm> alarms;
	
}
