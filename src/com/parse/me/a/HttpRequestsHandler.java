/*
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package com.parse.me.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;

public class HttpRequestsHandler {

	public String expId;
	public String times[];
	public static final int ALL_OK=1;
	public static final int NOT_OK=-1;

	/**
	 * 
	 * @param expId
	 * @return
	 * @throws Exception
	 */
	public String executeHttpGet(String expId) throws Exception {
		BufferedReader in = null;
		String page = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet();
			request.setURI(new URI("http://psyapp.co.uk/app/" + expId));
//			request.setURI(new URI("http://192.168.1.11:9003/app/" + expId));
			((AbstractHttpClient) client)
					.removeRequestInterceptorByClass(org.apache.http.protocol.RequestExpectContinue.class);
			HttpResponse response = client.execute(request);
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode == 200) {

				in = new BufferedReader(new InputStreamReader(response
						.getEntity().getContent()));
				StringBuilder stringBuilder = new StringBuilder();

				for (String line = null; (line = in.readLine()) != null;) {
					stringBuilder.append(line).append("\n");
				}
				in.close();
				client.getConnectionManager().shutdown();
				page = stringBuilder.toString();
				return page;
			} else if (statusCode == 404) {
				return "ERROR";
			} else {
				return "ERROR";
			}
		} finally {
			if (in != null) {
				try {
					in.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	// TODO This is the method for the post
	public void executeHttpPost(String expId, String log) {
		HttpClient client = new DefaultHttpClient();
		BasicHttpContext mHttpContext = new BasicHttpContext();

		HttpPost httpost = new HttpPost(
				"http://psyapp.co.uk/application/saveresult");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("expId", expId));
		nvps.add(new BasicNameValuePair("responseBody", log));
		try {
			httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mHttpContext.setAttribute(ClientContext.COOKIE_STORE,
				((AbstractHttpClient) client).getCookieStore().getCookies());

		try {
			HttpResponse response = client.execute(httpost, mHttpContext);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		client.getConnectionManager().shutdown();

	}

	public int postData(String id, String log) {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		((AbstractHttpClient) httpclient)
				.removeRequestInterceptorByClass(org.apache.http.protocol.RequestExpectContinue.class);
		// HttpPost httppost = new HttpPost(
		// "http://psyapp.co.uk/application/saveresult");
		HttpPost httppost = new HttpPost(
				"http://psyapp.co.uk/application/saveresult");

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("expId", id));
			nameValuePairs.add(new BasicNameValuePair("responseBody", log));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			int tries = 1;
			int postStatusCode = 0;
			// Execute HTTP Post Request
			int times = 0;
			while ((postStatusCode == 0 || postStatusCode != 200) &&  (times<=2) ) {
				HttpResponse response = httpclient.execute(httppost);
				postStatusCode = response.getStatusLine().getStatusCode();
				System.out.println("Post response code" + postStatusCode);
				times++;
			}
			httpclient.getConnectionManager().shutdown();
			return ALL_OK;
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			return NOT_OK;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return NOT_OK;
		}
	}
	
	
	public int SendPictureToServer(){
		
		
		return 0;
		
	}
}
