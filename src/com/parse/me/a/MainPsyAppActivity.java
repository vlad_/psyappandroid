package com.parse.me.a;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.PublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;
import com.parse.me.a.R;
import com.parse.me.a.Models.Alarm;
import com.parse.me.a.Models.Experiment;
import com.parse.me.a.Models.Frame;
import com.parse.me.a.Models.StoredExperiment;
import com.parse.me.a.Models.Trial;
import com.parse.me.database.DatabaseHandler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * @author Athanasios Anagnostopoulos
 * @version 0.1
 * 
 */
public class MainPsyAppActivity extends Activity implements OnClickListener {
	/** Called when the activity is first created. */

	TextView tv;
	Button button;
	Button db;
	Button nextExperimentButton;
	EditText et;
	boolean startNow = true;

	private ArrayList<ArrayList<String>> listOftrials;

	private ArrayList<Frame> frameArray;
	public String gpsEnabled;
	private String experimentId;
	String idOfExp;
	private int idOfAlarm = 0;
	private final static String TAG = "MainPsyAppActivity";
	private DatabaseHandler dbz;
	private String UID_FOR_NOT;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		tv = (TextView) findViewById(R.id.tv);
		et = (EditText) findViewById(R.id.expIdet);
		button = (Button) findViewById(R.id.button1);
		db = (Button) findViewById(R.id.dblistbut);
		nextExperimentButton = (Button) findViewById(R.id.nextButton);
		nextExperimentButton.setOnClickListener(this);
		button.setOnClickListener(this);
		db.setOnClickListener(this);
		listOftrials = new ArrayList<ArrayList<String>>();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		frameArray = new ArrayList<Frame>();

		dbz = new DatabaseHandler(this);
		if (dbz.getAllAlarms() == 0) {
			nextExperimentButton.setEnabled(false);
		} else {
			nextExperimentButton.setEnabled(true);
		}

		onNewIntent(getIntent());

	}

	/**
	 * In this method we start the activity that will show us the experiment. We
	 * actually create a bundle and map the values that we've previously
	 * retrieved from the server.Then we start the activity.
	 */
	public void sendContent() {

		Bundle parameters = new Bundle();

		parameters.putString("exp_id", this.experimentId);
		parameters.putString("gpsEnabled", this.gpsEnabled);
		parameters.putString("UID_FOR_NOT", this.UID_FOR_NOT);
		System.out.println("UID3:"+UID_FOR_NOT);
		Intent m2Exp = new Intent(MainPsyAppActivity.this, ExperimentView.class);
		m2Exp.putExtras(parameters);
		m2Exp.putExtra("Frames", frameArray);
		m2Exp.putExtra("Trials", listOftrials);

		startActivity(m2Exp);

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			if (isOnline() == true) {
				idOfExp = et.getText().toString();
				new LoadExperiment().execute(idOfExp);
				break;
			} else {
				CharSequence text = "Please connect to the internet";
				Toast toast = Toast.makeText(MainPsyAppActivity.this, text,
						Toast.LENGTH_LONG);
				toast.show();
			}
			break;
		case R.id.dblistbut:
			String state = Environment.getExternalStorageState();
			File folder = Environment.getExternalStorageDirectory();
			File dbFile = getApplicationContext().getDatabasePath(
					DatabaseHandler.getDATABASE_NAME());

			if (dbFile.exists()) {
				Log.v(TAG,
						"storage state is " + state + " and "
								+ folder.getPath() + "Database exists?"
								+ dbFile.exists());
				if (state.equals(Environment.MEDIA_MOUNTED)) {
					File psyappDirectory = new File(folder, "Psyapp");
					if (!psyappDirectory.exists()) {
						psyappDirectory.mkdirs();
					}
					startActivity(new Intent(MainPsyAppActivity.this,
							ListOfSavedExperiments.class));
				} else {
					Toast.makeText(
							getApplicationContext(),
							"Please insert external memory to save your experiment",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(
						getApplicationContext(),
						"Problem setting up the database.Clear Psyapp's data first and retry",
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.nextButton:

			if (isOnline() == true) {
				System.out.println("Calling :" + dbz.getLastAlarm()[0] + " OR "
						+ dbz.getLastAlarm()[1]);
				this.UID_FOR_NOT = dbz.getLastAlarm()[1];
				dbz.getAllAlarms();
				new LoadExperiment().execute(dbz.getLastAlarm()[0]);
				
				break;
			} else {
				CharSequence text = "Please connect to the internet";
				Toast toast = Toast.makeText(MainPsyAppActivity.this, text,
						Toast.LENGTH_LONG);
				toast.show();
			}
			break;
		default:
			break;
		}

	}

	public boolean isOnline() {

		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		return haveConnectedWifi || haveConnectedMobile;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_HOME && event.getRepeatCount() == 0) {
			this.moveTaskToBack(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * This class is an inner class that extends the AsyncTask and will do the
	 * hard work of pulling the data in the background.Also there is nothing to
	 * do apart from waiting that's why I'm viewing a progress dialog to notify
	 * the user for the progress of his request
	 * 
	 * @author deauth
	 * 
	 */
	public class LoadExperiment extends AsyncTask<String, Integer, String> {

		ProgressDialog progDialog;
		private String result;

		/**
		 * Before the request view the progress dialog
		 */
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progDialog = new ProgressDialog(MainPsyAppActivity.this);
			progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progDialog
					.setMessage("Downloading experiment's description file.Please make sure you have internet connection during the whole set up proceedure.");
			progDialog.setMax(10);
			progDialog.show();
			Log.i("onPreExecute", "mphke");
		}

		/**
		 * In the background call the link with the id specified by the user,
		 * retrieve the response,parse the json and finally put the values in
		 * arrays that will be used to view the experiment
		 */
		@Override
		protected String doInBackground(String... params) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(tv.getWindowToken(), 0);

			HttpRequestsHandler get = new HttpRequestsHandler();
			String serverResponse = null;

			// Execute the request and wait for the response which will be a
			// string
			try {
				serverResponse = get.executeHttpGet(params[0]);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (serverResponse != "ERROR" && serverResponse != null) {
				// Then we try to convert the String to a JSONObject to do what
				//
				Gson gson = new Gson();
				int frameCounter = 1;
				Experiment experiment = gson.fromJson(serverResponse,
						Experiment.class);

				experimentId = experiment.exp_id;
				gpsEnabled = experiment.exp_gpsEnabled;

				List<Alarm> alarms = experiment.alarms;

				for (Alarm alarm : alarms) {
					System.out.println("Alarm:" + alarm.date);
					System.out.println("Alarm:" + alarm.alarmClockType);
					System.out.println("Alarm:" + alarm.type);
					System.out.println("Alarm:" + alarm.nextExp);

					setAlarmClock(alarm);

				}

				ArrayList<String> tempStrings = new ArrayList<String>();
				for (Trial[] arrays : experiment.trials) {
					tempStrings = new ArrayList<String>();
					for (Trial id : arrays) {
						if (id.toString() != null && id.toString() != ""
								&& id.toString() != " ") {
							tempStrings.add(id.toString());
							System.out.println(id.toString());
						}
					}
					System.out.println("other array");
					listOftrials.add(tempStrings);

				}
				// List of the frames inside the object
				List<Frame> frames = experiment.frames;
				for (Frame indyFrame : frames) {

					Frame frame = new Frame(String.valueOf(frameCounter),
							indyFrame.frame_type, indyFrame.frame_text,
							indyFrame.frame_pic, indyFrame.frame_pic);
					if (indyFrame.frame_type.equalsIgnoreCase("normalFrame")) {
						frame.setTime(indyFrame.frame_timeLength);
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("optionFrame")) {
						frame.setb1(indyFrame.b1_text);
						frame.setb2(indyFrame.b2_text);
						frame.setB1option(indyFrame.b1_option);
						frame.setB2option(indyFrame.b2_option);
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("touchFrame")) {
						frame.setPresses(indyFrame.frame_presses);
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("freeTextFrame")) {
						frame.frame_pic = "n/a";
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("seekBarFrame")) {
						frame.frame_pic = "n/a";
						frame.setFrame_seekInit(indyFrame.frame_seekInit);
						frame.setFrame_seekMax(indyFrame.frame_seekMax);
						frame.setFrame_label1(indyFrame.frame_label1);
						frame.setFrame_label2(indyFrame.frame_label2);
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("checkBoxFrame")) {
						frame.frame_pic = "n/a";

						int g = 0;
						for (Object value : indyFrame.options) {
							frame.values[g] = value.toString();

							g++;
						}

					} else if (indyFrame.frame_type
							.equalsIgnoreCase("radioBoxFrame")) {
						frame.frame_pic = "n/a";

						int g = 0;
						for (Object value : indyFrame.options) {
							frame.values[g] = value.toString();

							g++;
						}
					} else if (indyFrame.frame_type
							.equalsIgnoreCase("spinnerFrame")) {
						frame.frame_pic = "n/a";
						frame.values = new String[indyFrame.options.size()];
						int g = 0;
						for (Object value : indyFrame.options) {
							frame.values[g] = value.toString();

							g++;
						}

					}

					// else if (indyFrame.frame_type
					// .equalsIgnoreCase("cameraFrame")) {
					//
					// }

					frameCounter++;
					System.out.println("frame_id" + frame.frame_id);
					System.out.println("frame_type" + frame.frame_type);
					System.out.println("frame_text" + frame.frame_text);
					System.out.println("frame_time" + frame.frame_timeLength);

					frameArray.add(frame);
					System.out.println("Frames num :" + frameArray.size());

				}

				publishProgress(2);

				this.result = "RESULT_OK";

				// Save experiment in the db
				DatabaseHandler db = new DatabaseHandler(
						getApplicationContext());

				System.out.println("Exists?:"
						+ db.checkIfExpExists(experimentId));
				if (db.checkIfExpExists(experimentId) == false) {
					StoredExperiment storedExperiment = new StoredExperiment(
							experimentId, Boolean.valueOf(gpsEnabled),
							serverResponse, false);
					db.addExperiment(storedExperiment);
				} else {
					Log.i(TAG, "DO NOT SAVE");
				}

				return "RESULT_OK";

			} else {
				progDialog.dismiss();
				return "SHIT_HAPPEND";
			}

		}

		/**
		 * On the end,if the result is retrieved properly go to the other
		 * activity
		 */
		protected void onPostExecute(String result) {

			if (result == "RESULT_OK") {
				sendContent();
			} else {
				CharSequence text = "The experiment that you've asked doesn't exist or something else went wrong";
				Toast toast = Toast.makeText(MainPsyAppActivity.this, text,
						Toast.LENGTH_LONG);
				toast.show();
			}
			progDialog.cancel();
		}

	}

	public void setAlarmClock(Alarm alarm) {

		// Calculate current time and alarm time
		Calendar myCal = Calendar.getInstance();
		Calendar nowCal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
		System.out.println("Trying to set up alarm");

		try {

			if (alarm.type.equalsIgnoreCase("simple")) {
				myCal.setTime(sdf.parse(alarm.date));
				System.out.println(myCal.get(Calendar.MINUTE) + " Vs "
						+ nowCal.get(Calendar.MINUTE));

				// Check if the alarm has passed
				if (myCal.after(nowCal)) {
					// Create a new PendingIntent and add it to the AlarmManager
					// TODO Change the 123 shit with a normal id
					Intent intent = new Intent(this,
							AlarmReceiverActivity.class);
					intent.putExtra("id", alarm.nextExp);
					PendingIntent pendingIntent = PendingIntent.getActivity(
							this, idOfAlarm, intent,
							PendingIntent.FLAG_CANCEL_CURRENT);
					AlarmManager am = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
					am.set(AlarmManager.RTC_WAKEUP, myCal.getTimeInMillis(),
							pendingIntent);
					System.out.println("ALARM CLOCK SET");

					if (alarm.alarmClockType.equalsIgnoreCase("chain")) {
						String experimentLog = null;
						HttpRequestsHandler get = new HttpRequestsHandler();
						String serverResponse = null;
						try {
							experimentLog = get.executeHttpGet(alarm.nextExp);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						// Save experiment in the db
						DatabaseHandler db = new DatabaseHandler(
								getApplicationContext());

						System.out.println("Exists?:"
								+ db.checkIfExpExists(alarm.nextExp));
						if (db.checkIfExpExists(alarm.nextExp) == false) {
							StoredExperiment storedExperiment = new StoredExperiment(
									alarm.nextExp, Boolean.valueOf(gpsEnabled),
									experimentLog, false);
							db.addExperiment(storedExperiment);

						} else {
							Log.i(TAG, "DO NOT SAVE");
						}
					}
				}
			} else if (alarm.type.equalsIgnoreCase("relative")) {
				myCal.add(Calendar.MINUTE, Integer.parseInt(alarm.date));
				Intent intent = new Intent(this, AlarmReceiverActivity.class);
				intent.putExtra("id", alarm.nextExp);
				PendingIntent pendingIntent = PendingIntent.getActivity(this,
						idOfAlarm, intent, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager am = (AlarmManager) getSystemService(Activity.ALARM_SERVICE);
				am.set(AlarmManager.RTC_WAKEUP, myCal.getTimeInMillis(),
						pendingIntent);
				System.out.println("ALARM CLOCK SET");

				if (alarm.alarmClockType.equalsIgnoreCase("chain")) {
					String experimentLog = null;
					HttpRequestsHandler get = new HttpRequestsHandler();
					String serverResponse = null;
					try {
						experimentLog = get.executeHttpGet(alarm.nextExp);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// Save experiment in the db
					DatabaseHandler db = new DatabaseHandler(
							getApplicationContext());

					System.out.println("Exists?:"
							+ db.checkIfExpExists(alarm.nextExp));
					if (db.checkIfExpExists(alarm.nextExp) == false) {
						StoredExperiment storedExperiment = new StoredExperiment(
								alarm.nextExp, Boolean.valueOf(gpsEnabled),
								experimentLog, false);
						db.addExperiment(storedExperiment);

					} else {
						Log.i(TAG, "DO NOT SAVE");
					}
				}
			}
			idOfAlarm++;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		this.listOftrials.clear();
		this.frameArray.clear();
	}

	/**
	 * When the activity is finished empty all the arrays
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		listOftrials.clear();
		this.frameArray.clear();
		this.idOfAlarm = 0;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		super.onResume();

		if (dbz.getAllAlarms() == 0) {
			nextExperimentButton.setEnabled(false);
		} else {
			nextExperimentButton.setEnabled(true);
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		// Try to write on the edit text the number of the experiment
		// that was asked from another part of the program so the user
		// can just download the experiment

		Bundle extras = intent.getExtras();
		if (extras != null) {

			if (extras.containsKey("ExpId")) {
				String idToBePulled = extras.getString("ExpId");
				System.out.println("Contains HINT:" + idToBePulled);
				et.setText(idToBePulled);
			}
			
			if (extras.containsKey("UID")) {
				String uid = extras.getString("UID");
				System.out.println("UID:"+uid);
				this.UID_FOR_NOT = uid;
			}
		}
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();

	}

}